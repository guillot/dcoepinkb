name := "DCoEPinKB"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.4.3",
  "org.apache.spark" %% "spark-sql" % "2.4.3",
  "net.debasishg" %% "redisclient" % "3.30",
  "com.opencsv" % "opencsv" % "5.4"
)

dependencyOverrides ++= Seq(
  "io.netty" % "netty" % "3.9.9.Final",
  "com.google.code.findbugs" % "jsr305" % "3.0.2",
  "com.google.guava" % "guava" % "16.0.1"
)
