package pathrankingmeasures

import models.Path
import org.apache.commons.codec.digest.DigestUtils
import queryexecutor.SparkSQLQueryExecutor
import utils.RedisConnection
import utils.Serialization.deserialize

import scala.collection.mutable

/**
 * Created by javier on 14/04/21.
 *
 * @author javier
 * @version 1.0 14/04/21
 * @see pathrankingmeasures.RelationshipPathRankingMeasure
 */
class PMI(var sparkQueryExecutor: SparkSQLQueryExecutor) extends RelationshipPathRankingMeasure with Serializable {

  def getPathScore(path: Path): Double = {
    val pathHash: String = String.format("%s:PMI:%s", sparkQueryExecutor.parametersHash, DigestUtils.sha256Hex(path.toString))
    if (RedisConnection.redisClient.get(pathHash).isEmpty) {
      var score: Double = 0
      for (triple <- path.steps) {
        val step = sparkQueryExecutor.getTripleCorrectDirection(triple)
        var pmi1: Double = Math.log10(sparkQueryExecutor.getFrequencyOfPropertyOutgoingFromResource(step.sub, step.prop) / (sparkQueryExecutor.getFrequencyOfEntity(step.sub) * sparkQueryExecutor.getFrequencyOfProperty(step.prop)))
        var pmi2: Double = Math.log10(sparkQueryExecutor.getFrequencyOfPropertyIncomingToResource(step.obj, step.prop) / (sparkQueryExecutor.getFrequencyOfProperty(step.prop) * sparkQueryExecutor.getFrequencyOfEntity(step.obj)))
        if (pmi1.isNegInfinity) pmi1 = 0
        if (pmi2.isNegInfinity) pmi2 = 0
        score += pmi1 + pmi2
      }
      var median: Double = 0
      if (path.steps.size > 1) {
        val pmi = new mutable.ListBuffer[Double]
        for (i <- 0 until (path.steps.size - 1)) {
          val prop1 = path.steps(i).prop
          val prop2 = path.steps(i + 1).prop
          var pmiProp: Double = Math.log10(sparkQueryExecutor.getCountOfPropertiesCoOccurrence(prop1, prop2).toDouble / (sparkQueryExecutor.getFrequencyOfProperty(prop1) * sparkQueryExecutor.getFrequencyOfProperty(prop2)))
          if (pmiProp.isNegInfinity) pmiProp = 0
          pmi.append(pmiProp)
        }
        val sortedPMI = pmi.sorted
        val middle = sortedPMI.size / 2
        median = if (sortedPMI.size % 2 == 1) sortedPMI(middle)
        else (sortedPMI(middle - 1) + sortedPMI(middle)) / 2.0
      }
      score = median + (score / (2 * path.steps.size))
      if (score < 0) score = 0
      RedisConnection.setValueInRedis(pathHash, score)
      score
    }
    else deserialize(RedisConnection.redisClient.get(pathHash).get).asInstanceOf[Double]
  }
}
