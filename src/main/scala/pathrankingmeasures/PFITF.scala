package pathrankingmeasures

import models.Path
import org.apache.commons.codec.digest.DigestUtils
import queryexecutor.SparkSQLQueryExecutor
import utils.RedisConnection
import utils.Serialization.deserialize

/**
 * Created by javier on 14/04/21.
 *
 * @author javier
 * @version 1.0 14/04/21
 * @see pathrankingmeasures.RelationshipPathRankingMeasure
 */
class PFITF(var sparkQueryExecutor: SparkSQLQueryExecutor) extends RelationshipPathRankingMeasure with Serializable {

  def getPathScore(path: Path): Double = {
    val pathHash: String = String.format("%s:PFITF:%s", sparkQueryExecutor.parametersHash, DigestUtils.sha256Hex(path.toString))
    if (RedisConnection.redisClient.get(pathHash).isEmpty) {
      var score: Double = 0
      for (triple <- path.steps) {
        val step = sparkQueryExecutor.getTripleCorrectDirection(triple)
        val pfo = sparkQueryExecutor.getCountOfPropertyOutgoingLinksFromResource(step.sub, step.prop).toDouble / sparkQueryExecutor.getCountOfAllOutgoingLinksFromResource(step.sub)
        val pfi = sparkQueryExecutor.getCountOfPropertyIncomingLinksToResource(step.obj, step.prop).toDouble / sparkQueryExecutor.getCountOfAllIncomingLinksToResource(step.obj)
        val itf = Math.log10(sparkQueryExecutor.getTotalNumberOfTriples.toDouble / sparkQueryExecutor.getTotalNumberOfTriplesWithProperty(step.prop))
        score += (itf * (pfo + pfi)) / 2
      }
      if (score > 0) score = score / path.steps.size else score = 0
      RedisConnection.setValueInRedis(pathHash, score)
      score
    }
    else deserialize(RedisConnection.redisClient.get(pathHash).get).asInstanceOf[Double]
  }
}
