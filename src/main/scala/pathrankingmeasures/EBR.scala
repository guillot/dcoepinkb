package pathrankingmeasures

import models.Path
import org.apache.commons.codec.digest.DigestUtils
import queryexecutor.SparkSQLQueryExecutor
import utils.RedisConnection
import utils.Serialization.deserialize

/**
 * Created by javier on 14/04/21.
 *
 * @author javier
 * @version 1.0 14/04/21
 * @see pathrankingmeasures.RelationshipPathRankingMeasure
 */
class EBR(var sparkQueryExecutor: SparkSQLQueryExecutor) extends RelationshipPathRankingMeasure with Serializable {

  def getPathScore(path: Path): Double = {
    val pathHash: String = String.format("%s:EBR:%s", sparkQueryExecutor.parametersHash, DigestUtils.sha256Hex(path.toString))
    if (RedisConnection.redisClient.get(pathHash).isEmpty) {
      var score: Double = path.steps.map(triple => sparkQueryExecutor.getTripleCorrectDirection(triple))
        .map(step => sparkQueryExecutor.getCountOfPropertyOutgoingLinksFromResource(step.sub, step.prop) + sparkQueryExecutor.getCountOfPropertyIncomingLinksToResource(step.obj, step.prop) - 1)
        .sum
      if (score > 0) score = 1 / score else score = 0
      RedisConnection.setValueInRedis(pathHash, score)
      score
    }
    else deserialize(RedisConnection.redisClient.get(pathHash).get).asInstanceOf[Double]
  }
}