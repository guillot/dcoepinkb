package pathrankingmeasures

import models.{Path, PathRanking}

/**
 * A trait to be implemented by those classes that are relationship-path ranking measures
 * Created by javier on 14/04/21.
 *
 * @author javier
 * @version 1.0 14/04/21
 */
trait RelationshipPathRankingMeasure {
  /**
   * Method that returns a list of paths ordered by a relationship-path ranking measure
   *
   * @param paths a list of paths to be ordered by a relationship-path ranking measure
   * @return a list of paths ordered by a relationship-path ranking measure
   */
  def getPathsOrderedByScore(paths: Set[Path]): List[PathRanking] = {
    paths.toList.map(path => PathRanking(path, getPathScore(path)))
      .filter(_.score > 0)
      .sortBy(_.score)(Ordering[Double].reverse)
  }

  def getPathScore(path: Path): Double
}
