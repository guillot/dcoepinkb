package main

import com.opencsv.CSVWriter
import org.apache.spark.sql.functions.{avg, count}
import org.apache.spark.sql.{DataFrame, SparkSession}
import scala.collection.JavaConverters.asJavaIterableConverter

import java.io.{BufferedWriter, FileWriter}
import scala.collection.mutable.ListBuffer

object GetKBStatsApp extends App {
  val spark: SparkSession = SparkSession
    .builder()
    .appName("Get KB Stats App")
    .master("local[*]")
    .getOrCreate()

  import spark.implicits._

  private val dbpediaDF: DataFrame = spark.read.parquet("src/main/resources/dbpedia/mappingbased_objects_en.parquet")
  private val cachedDBpediaDF: DataFrame = dbpediaDF.cache()

  val triplesCount = cachedDBpediaDF.count()
  val subjects = cachedDBpediaDF.select($"sub" as "iri").distinct()
  val subjectsCount = subjects.count()
  val objects = cachedDBpediaDF.select($"obj" as "iri").distinct()
  val objectsCount = objects.count()
  val subsIntersectObjs = subjects.intersect(objects)
  val subsIntersectObjsCount = subsIntersectObjs.count()
  val subsUnionObjs = subjects.union(objects).distinct()
  val subsUnionObjsCount = subsUnionObjs.count()
  val properties = cachedDBpediaDF.select($"prop").distinct()
  val propertiesCount = properties.count()
  val avgOutDegree = cachedDBpediaDF.groupBy($"sub").agg(count("*").as("cnt")).agg(avg($"cnt").as("outAVG")).select($"outAVG").collectAsList()
  val avgInDegree = cachedDBpediaDF.groupBy($"obj").agg(count("*").as("cnt")).agg(avg($"cnt").as("inAVG")).select($"inAVG").collectAsList()
  println("triplesCount: " + triplesCount)
  println("subjectsCount: " + subjectsCount)
  println("objectsCount: " + objectsCount)
  println("subsIntersectObjsCount: " + subsIntersectObjsCount)
  println("subsUnionObjsCount: " + subsUnionObjsCount)
  println("propertiesCount: " + propertiesCount)
  println("avgOutDegree: " + avgOutDegree.get(0).getDouble(0))
  println("avgInDegree: " + avgInDegree.get(0).getDouble(0))

  val datasetsNames = List("dbpedia_21M.parquet", "dbpedia_45M.parquet")
  val outputFile = new BufferedWriter(new FileWriter("out/entities-degrees.csv"))
  val csvWriter = new CSVWriter(outputFile)
  val csvFields = Array("Entity IRI", "Dataset", "Outgoing links count", "Incoming links count")
  val listOfRecords = new ListBuffer[Array[String]]()
  listOfRecords += csvFields
  val entities = List("http://dbpedia.org/resource/Michael_Jackson", "http://dbpedia.org/resource/Whitney_Houston", "http://dbpedia.org/resource/The_Beatles",	"http://dbpedia.org/resource/The_Rolling_Stones", "http://dbpedia.org/resource/Elton_John", "http://dbpedia.org/resource/George_Michael", "http://dbpedia.org/resource/Led_Zeppelin", "http://dbpedia.org/resource/The_Who", "http://dbpedia.org/resource/Pink_Floyd", "http://dbpedia.org/resource/David_Gilmour", "http://dbpedia.org/resource/Elizabeth_Taylor", "http://dbpedia.org/resource/Richard_Burton", "http://dbpedia.org/resource/Cary_Grant", "http://dbpedia.org/resource/Katharine_Hepburn", "http://dbpedia.org/resource/Laurence_Olivier", "http://dbpedia.org/resource/Ralph_Richardson", "http://dbpedia.org/resource/Errol_Flynn", "http://dbpedia.org/resource/Olivia_de_Havilland", "http://dbpedia.org/resource/William_Powell", "http://dbpedia.org/resource/Myrna_Loy")
  for (datasetName <- datasetsNames) {
    val dataset = spark.read.parquet(datasetName).cache()
    for (entity <- entities) {
      val outDegree = dataset.filter($"sub" === entity).count()
      val inDegree = dataset.filter($"obj" === entity).count()
      listOfRecords += Array(entity, datasetName, outDegree.toString, inDegree.toString)
    }
  }
  csvWriter.writeAll(listOfRecords.asJava)
  csvWriter.close()
  outputFile.close()

  spark.stop()
}
