package main

import activationfunctions.EntitySimilarityMeasure
import com.opencsv.CSVWriter
import org.apache.spark.sql.SparkSession
import pathrankingmeasures.RelationshipPathRankingMeasure
import queryexecutor.SparkSQLQueryExecutor
import searchstrategy.BackwardSearch

import java.io.{BufferedWriter, FileNotFoundException, FileWriter, IOException}
import scala.collection.JavaConverters.asJavaIterableConverter
import scala.collection.mutable.ListBuffer
import scala.io.Source

object MySparkSession {
  val spark: SparkSession = SparkSession
    .builder()
    .appName("Distributed CoEPinKB App")
    .master("local[*]")
    .config("spark.driver.maxResultSize", "2g")
    .getOrCreate()
}

object DCoEPinKBApp {
  def main(args: Array[String]) {
    val inputFilename = args(0)
    val outputFilename = args(1)

    try {
      val bufferedInputFile = Source.fromFile(inputFilename)
      val lines = bufferedInputFile.getLines.toArray
      val entities = lines(0).split(" ")
      val inputKBPath = lines(1)
      val entitiesPrefix = lines(2)
      val generalArgs = lines(3).split(" ")
      val (maxPathLength, maxEntityDegree, expansionLimit) = (generalArgs(0), generalArgs(1), generalArgs(2))
      val propertiesToBeIgnored = lines(4).split(" ").toSet
      val entitiesSimilarityMeasureName = lines(5)
      val activationFunctionArgs = lines(6).split(" ").toList
      val pathRankingMeasureName = lines(7)
      val topKPaths = lines(8)
      val numberOfExecutions = lines(9)

      val sparkSQLQueryExecutor = new SparkSQLQueryExecutor(inputKBPath, entitiesPrefix, propertiesToBeIgnored)
      val activationFunction = activationFunctionBuilder(entitiesSimilarityMeasureName, sparkSQLQueryExecutor, activationFunctionArgs)
      val pathRankingMeasure = pathRankingMeasureBuilder(pathRankingMeasureName, sparkSQLQueryExecutor)

      val outputFile = new BufferedWriter(new FileWriter(outputFilename))
      val csvWriter = new CSVWriter(outputFile)
      val csvFields = Array("# Execution", "Entity 1", "Entity 2", "Entities Prefix", "Maximum Path Length",
        "Maximum Entity Degree", "Expansion Limit", "Properties to be ignored",
        "Entity Similarity Measure", "Entity Similarity Measure Args", "Relationship Path Ranking Measure",
        "Top-k Paths", "Backward Search Time", "Candidate Paths Count", "Candidate Paths",
        "Ranking Paths Time", "Resulting Paths Count", "Resulting Paths")
      val listOfRecords = new ListBuffer[Array[String]]()
      listOfRecords += csvFields
      for (i <- 1 to numberOfExecutions.toInt) {
        val backwardSearch = new BackwardSearch(entities(0), entities(1), maxPathLength.toInt, maxEntityDegree.toInt, expansionLimit.toDouble, activationFunction, sparkSQLQueryExecutor)
        var start = System.currentTimeMillis
        val resultingPaths = backwardSearch.execute
        val backwardSearchTime = System.currentTimeMillis - start

        start = System.currentTimeMillis
        val relevantPaths = pathRankingMeasure.getPathsOrderedByScore(resultingPaths)
        val rankingPathsTime = System.currentTimeMillis - start
        val rankedPaths = relevantPaths.take(Math.min(relevantPaths.size, topKPaths.toInt))

        listOfRecords += Array(i.toString, entities(0), entities(1), entitiesPrefix, maxPathLength, maxEntityDegree,
          expansionLimit, propertiesToBeIgnored.toString, entitiesSimilarityMeasureName,
          activationFunctionArgs.toString, pathRankingMeasureName, topKPaths, backwardSearchTime.toString,
          resultingPaths.size.toString, resultingPaths.toString, rankingPathsTime.toString, rankedPaths.size.toString,
          rankedPaths.toString)
      }
      csvWriter.writeAll(listOfRecords.asJava)
      csvWriter.close()
      outputFile.close()
      bufferedInputFile.close
    } catch {
          case _: FileNotFoundException => println("Couldn't find that file.")
          case _: IOException => println("Got an IOException!")
    }
    MySparkSession.spark.close()
  }

  def activationFunctionBuilder(name: String, sparkQueryExecutor: SparkSQLQueryExecutor, activationFunctionArgs: List[String]): EntitySimilarityMeasure = {
    val activationFunctionClass = Class.forName("activationfunctions." + name)
    activationFunctionClass.getConstructors.head.newInstance(sparkQueryExecutor, activationFunctionArgs).asInstanceOf[EntitySimilarityMeasure]
  }

  def pathRankingMeasureBuilder(name: String, sparkQueryExecutor: SparkSQLQueryExecutor): RelationshipPathRankingMeasure = {
    val pathRankingMeasureClass = Class.forName("pathrankingmeasures." + name)
    pathRankingMeasureClass.getConstructors.head.newInstance(sparkQueryExecutor).asInstanceOf[RelationshipPathRankingMeasure]
  }
}