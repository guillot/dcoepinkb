package models

/**
 * Created by javier on 30/03/21.
 *
 * @author javier
 * @version 1.0 30/03/21
 */

case class Triple(sub: String, prop: String, obj: String)

case class Similarity(entity1: String, entity2: String, similarity: Double)

case class Path(steps: List[Triple])

case class PathRanking(path: Path, score: Double)
