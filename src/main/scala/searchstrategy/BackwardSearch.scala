package searchstrategy

import main.MySparkSession.spark.implicits._
import activationfunctions.EntitySimilarityMeasure
import models._
import org.apache.spark.sql.functions.lit
import queryexecutor.SparkSQLQueryExecutor
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * Class that implements a Backward Search Algorithm to find relationship paths
 * between a given pair of entities in a knowledge base
 * Created by javier on 30/03/21.
 *
 * @param sourceEntityIRI     a String containing the IRI of an entity
 * @param targetEntityIRI     a String containing the IRI of an entity
 * @param maxPathLength       an integer representing the maximum length that will be explored to find a relationship path between the two entities provided
 * @param maxEntityDegree     an integer representing the maximum degree (number of links) of an entity that will be accepted to expand an entity
 * @param activationFunction  an entity similarity measure (a class that implements IEntitySimilarityMeasure) to be used by the Backward Search Algorithm
 * @param expansionLimit      a real number between 0 and 1 defining an expansion limit. If the value equals 0 then the algorithm will expand only the top-10 more similar neighbors for each entity
 * @param sparqlQueryExecutor a SPARQLQueryExecutor to execute the queries in the selected data set
 * @see IEntitySimilarityMeasure
 *
 * @author javier
 * @version 1.0 30/03/21
 */
class BackwardSearch(var sourceEntityIRI: String, var targetEntityIRI: String, var maxPathLength: Int = 4, var maxEntityDegree: Int = 200, var expansionLimit: Double = 0.5, var activationFunction: EntitySimilarityMeasure, var sparqlQueryExecutor: SparkSQLQueryExecutor) {
  // Auxiliary structures
  private var expandedEntities: Set[String] = Set()
  private var highDegreeEntities: Set[String] = Set()
  private val partialPaths: Array[mutable.Map[String, Set[String]]] = Array(mutable.Map(), mutable.Map()) // First set contains partial paths from the left to the right (source -> target), and second set contains partial paths from the right to the left (target -> source)

  /**
   * Method that execute the Backward Search Algorithm
   *
   * @return a set of strings containing the relationship paths between two entities provided in the constructor of the searchstrategy.BackwardSearch class
   */
  def execute: Set[Path] = {
    var side: Int = 0 // 0: left; 1: right;
    val entitiesToVisit: Array[Set[String]] = Array(Set(sourceEntityIRI), Set(targetEntityIRI)) // First set for entities to visit from the left, and second set for entities to visit  from the right
    var expanding = 0
    while (expanding < maxPathLength) {
      entitiesToVisit(side) = expansion(entitiesToVisit(side), partialPaths(side))
      expanding = expanding + 1
      side = expanding % 2
    }
    intersection()
  }

  /**
   * Method that applies the activation function to a set of entities and builds a set of sub paths that links resources
   *
   * @param entitiesToVisit    a set of entities that will be expanded (for each entity it will be applied the activation function to select the most similar entities)
   * @param partialPaths       a set of sub paths that links resources
   * @return a set of resource IRIs that will be expanded in a next expansion call
   * @see IEntitySimilarityMeasure
   */
  private def expansion(entitiesToVisit: Set[String], partialPaths: mutable.Map[String, Set[String]]): Set[String] = {
    var newEntitiesToVisit: Set[String] = Set()
    // Remove entities that were previously expanded
    val entitiesToExpand = entitiesToVisit.diff(expandedEntities)
    if (entitiesToExpand.nonEmpty) {
      val allNeighbors = entitiesToExpand.map(entity => sparqlQueryExecutor.getNeighborsOfResource(entity).select(lit(entity).as("sub"), $"prop", $"obj"))
        .reduce(_ union _)
        .collect()
      for (node <- entitiesToExpand) {
        val neighborsOfNode = allNeighbors.filter(row => row(0) == node)
        // If the entity is the source or target entity or has less than maxEntityDegree links then it will be expanded
        if (node == sourceEntityIRI || node == targetEntityIRI || neighborsOfNode.length <= maxEntityDegree) {
          val activatedNodes = activationFunction.getSimilarity(node, neighborsOfNode.map(row => row(2).asInstanceOf[String]).distinct)
          expandedEntities += node
          if (activatedNodes.nonEmpty) {
            // Only the portion of more similar entities specified by the user are expanded, or the top-k more similar entities
            for (adjacentNode <- activatedNodes.take(if (0 < expansionLimit && expansionLimit < 1) (activatedNodes.length * expansionLimit).toInt else Math.abs(expansionLimit).toInt)) {
              val objectIRI = adjacentNode.entity2
              if (!expandedEntities.contains(objectIRI)) {
                val triples = neighborsOfNode.filter(row => row(2) == objectIRI)
                triples.foreach(triple => {
                  val step = String.format("%s %s %s", node, triple.getString(1), objectIRI)
                  var subPaths: Set[String] = if (partialPaths.contains(objectIRI)) partialPaths(objectIRI) else Set()
                  if (partialPaths.contains(node)) {
                    for (followPath <- partialPaths(node)) {
                      if (followPath.split(" ").length / 3 < maxPathLength)
                        subPaths += String.format("%s %s", followPath, step)
                    }
                  }
                  else subPaths += step
                  partialPaths(objectIRI) = subPaths
                  if (!highDegreeEntities.contains(objectIRI))
                    newEntitiesToVisit += objectIRI
                })
              }
            }
          }
        }
        else
          highDegreeEntities += node
      }
    }
    newEntitiesToVisit
  }

  /**
   * Method that generates single paths between the two entities provided by
   * the user in the constructor of the searchstrategy.BackwardSearch class.
   * It search when it is possible to make a join between two sub paths that start one in the source entity and the other in the target entity.
   */
  private def intersection(): Set[Path] = {
    var resultingPaths: Set[Path] = Set()
    for (entityIRI <- partialPaths(0).keySet) {
      if (partialPaths(1).contains(entityIRI)) { // Two sub-paths coming one from left and the other from right reach the same entity (entityIRI),
        // so they are joined into a single path
        resultingPaths ++= convertStringPathsToTriples(joinSubPaths(partialPaths(0)(entityIRI), reversePaths(partialPaths(1)(entityIRI))))
      }
    }
    if (partialPaths(0).contains(targetEntityIRI)) { // A path coming from left reaches the target entity
      resultingPaths ++= convertStringPathsToTriples(partialPaths(0)(targetEntityIRI))
    }
    if (partialPaths(1).contains(sourceEntityIRI)) { // A path coming from right reaches the source entity
      // Paths from right to left must be reversed
      resultingPaths ++= convertStringPathsToTriples(reversePaths(partialPaths(1)(sourceEntityIRI)))
    }
    resultingPaths
  }

  /**
   * Method that joins two sub paths that start one in the source entity and the other in the target entity.
   *
   * @param partialPathsFromLeft  a set of sub paths that start in the source entity (from the left)
   * @param partialPathsFromRight a set of sub paths that start in the target entity (from the right)
   * @return a set of single paths generated by multiple joins between sub paths that start in the source entity and in the target entity.
   */
  private def joinSubPaths(partialPathsFromLeft: Set[String], partialPathsFromRight: Set[String]): Set[String] = {
    var joinedSubPaths: Set[String] = Set()
    for (partialPathFromLeft <- partialPathsFromLeft) {
      for (partialPathFromRight <- partialPathsFromRight) {
        joinedSubPaths += partialPathFromLeft + " " + partialPathFromRight
      }
    }
    joinedSubPaths
  }

  /**
   * Method that reverse a string representing a path between two entities (to change orientation of the path)
   *
   * @param paths a set of paths to be reversed
   * @return a set of paths that were reversed (the orientation of the paths were reversed)
   */
  private def reversePaths(paths: Set[String]): Set[String] = {
    paths.map(_.reverse.split(" ", -1)
      .map(_.reverse)
      .mkString(" "))
  }

  /**
   * Method that convert a set of strings representing paths in a list of triples (each triple, representing one RDF statement)
   *
   * @param paths a set of strings representing paths
   * @return a set of lists of triples representing the statements present in the paths
   */
  private def convertStringPathsToTriples(paths: Set[String]): Set[Path] = {
    var resultingPathsSplitted: Set[Path] = Set()
    for (path <- paths) {
      val currentPath: ListBuffer[Triple] = ListBuffer[Triple]()
      val splittedPath = path.split(" ")
      var i = 0
      while (i * 3 < splittedPath.length) {
        currentPath.append(models.Triple(splittedPath(i * 3), splittedPath(i * 3 + 1), splittedPath(i * 3 + 2)))
        i += 1
      }
      if (currentPath.size <= maxPathLength)
        resultingPathsSplitted += Path(currentPath.toList)
    }
    resultingPathsSplitted
  }
}