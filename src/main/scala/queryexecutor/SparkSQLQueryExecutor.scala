package queryexecutor

import main.MySparkSession.spark
// For implicit conversions like converting RDDs to DataFrames
import spark.implicits._
import utils.RedisConnection
import utils.Serialization.deserialize
import models.Triple
import org.apache.commons.codec.digest.DigestUtils
import org.apache.spark.sql.functions.{col, countDistinct, not}
import org.apache.spark.sql.DataFrame

class SparkSQLQueryExecutor(var inputKBPath: String, var entitiesPrefix: String, var propertiesToBeIgnored: Set[String]) extends Serializable {
  val parametersHash: String = DigestUtils.sha256Hex(String.format("DCoEPinKB:%s:%s:%s", inputKBPath, entitiesPrefix, propertiesToBeIgnored.mkString(" ")))

  private val dbpediaDF: DataFrame = spark.read.parquet(inputKBPath).cache()
  private val cachedDBpediaDF: DataFrame = dbpediaDF.filter(not($"prop".isInCollection(propertiesToBeIgnored))).cache()
  private val triplesCount = dbpediaDF.count()
  private val subjects = dbpediaDF.select($"sub" as "iri").distinct()
  private val objects = dbpediaDF.filter($"obj".startsWith(entitiesPrefix)).select($"obj" as "iri").distinct()
  private val entitiesCount = subjects.union(objects).distinct().count

  def getNeighborsOfResource(resourceIRI: String): DataFrame = {
    val entityRedisKey = String.format("%s:neighborsOf:%s", parametersHash, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val neighbors = getOutgoingLinksFromResourceToEntities(resourceIRI).union(getIncomingLinksToResource(resourceIRI)).dropDuplicates()
      RedisConnection.setValueInRedis(entityRedisKey, neighbors.collect())
      neighbors
    }
    else RedisConnection.getValueFromRedisAsDF(entityRedisKey, "prop", "obj")
  }

  /**
   * Method that returns the RDF statements (ignoring the subject) where the resource is the subject and the object is a URI (property values are ignored),
   * but only those statements where the property is not included in the propertiesToBeIgnored set
   *
   * @param resourceIRI a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @return a DataFrame containing the content of the resource (RDF statements where the resource is the subject),
   *         but only those statements where the property is not included in the @param propertiesToBeIgnored
   */
  def getOutgoingLinksFromResourceToEntities(resourceIRI: String): DataFrame = {
    val entityRedisKey = String.format("%s:outgoing:links:from:%s", parametersHash, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val outgoingLinks = cachedDBpediaDF.filter($"sub" === resourceIRI && $"obj".startsWith(entitiesPrefix))
        .select($"prop", $"obj")
        .dropDuplicates()
      RedisConnection.setValueInRedis(entityRedisKey, outgoingLinks.collect())
      outgoingLinks
    }
    else RedisConnection.getValueFromRedisAsDF(entityRedisKey, "prop", "obj")
  }

  /**
   * Method that returns the RDF statements (ignoring the subject) where the resource is the subject
   * and the object is at an specified depth from the subject, but only those statements
   * where the properties are not included in the propertiesToBeIgnored set
   *
   * @param resourceIRI   a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @param depth an integer representing the depth at which the RDF statements were selected
   * @return a ResultSet containing the content of the resource (RDF statements where the resource is the subject),
   *         but only those statements where the property is not included in the @param propertiesToBeIgnored
   */
  def getReachableEntitiesFromResourceAtDepth(resourceIRI: String, depth: Int): Set[String] = {
    if (depth >= 1) {
      val entityRedisKey = String.format("%s:reachable:entities:from:%s:at:depth:%d", parametersHash, resourceIRI, depth.asInstanceOf[Integer])
      if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
        var reachableEntitiesDF = getOutgoingLinksFromResourceToEntities(resourceIRI).select($"obj").distinct()
        var reachableEntities = reachableEntitiesDF.map(r => r.getString(0).replace(entitiesPrefix, "")).collect
        if (depth > 1) {
          for (i <- 1 until depth) {
            val (aliasDF1, aliasDF2) = ("df" + i, "df" + (i + 1))
            val (colDF1, colDF2) = (aliasDF1 + ".obj", aliasDF2 + ".sub")
            reachableEntitiesDF = reachableEntitiesDF.as(aliasDF1)
              .join(cachedDBpediaDF.as(aliasDF2),
              col(colDF1) === col(colDF2), "inner")
              .filter(col(aliasDF2 + ".obj").startsWith(entitiesPrefix))
              .select(col(aliasDF2 + ".obj"))
              .distinct()
            reachableEntities ++= reachableEntitiesDF.map(r => r.getString(0).replace(entitiesPrefix, "")).collect
          }
        }
        val reachableEntitiesSet = reachableEntities.toSet
        RedisConnection.setValueInRedis(entityRedisKey, reachableEntitiesSet)
        reachableEntitiesSet
      }
      else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Set[String]]
    }
    else
      Set()
  }

  /**
   * Method that returns the RDF statements (ignoring the object) where the resource is the object
   *
   * @param resourceIRI a resource IRI
   * @return a DataFrame containing the content of the resource (RDF statements where the resource is the object),
   *         but only those statements where the property is not included in the @param propertiesToBeIgnored
   */
  def getIncomingLinksToResource(resourceIRI: String): DataFrame = {
    val entityRedisKey = String.format("%s:incoming:links:to:%s", parametersHash, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val incomingLinks = cachedDBpediaDF.filter($"obj" === resourceIRI)
        .select($"prop", $"sub" as "obj")
        .dropDuplicates()
      RedisConnection.setValueInRedis(entityRedisKey, incomingLinks.collect())
      incomingLinks
    }
    else RedisConnection.getValueFromRedisAsDF(entityRedisKey, "prop", "obj")
  }

  /**
   * Method that returns the number of RDF statements where the resource is the subject and the property
   *
   * @param resourceIRI a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @param propertyIRI a property IRI that will be used to filter statements
   * @return an integer representing
   */
  def getCountOfPropertyIncomingLinksToResource(resourceIRI: String, propertyIRI: String): Long = {
    val entityRedisKey = String.format("%s:count:%s:incoming:%s", parametersHash, propertyIRI, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val count = cachedDBpediaDF.filter($"obj" === resourceIRI && $"prop" === propertyIRI).count()
      RedisConnection.setValueInRedis(entityRedisKey, count)
      count
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns the number of RDF statements where the resource is the object,
   * but only those statements where the property is not included in the propertiesToBeIgnored set
   *
   * @param resourceIRI a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @return an integer representing
   */
  def getCountOfAllIncomingLinksToResource(resourceIRI: String): Long = {
    val entityRedisKey = String.format("%s:count:all:incoming:%s", parametersHash, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val count = cachedDBpediaDF.filter($"obj" === resourceIRI).count()
      RedisConnection.setValueInRedis(entityRedisKey, count)
      count
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns the number of RDF statements where the resource is the subject and the property is
   *
   * @param resourceIRI a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @param propertyIRI a property IRI that will be used to filter statements
   * @return an integer representing
   */
  def getCountOfPropertyOutgoingLinksFromResource(resourceIRI: String, propertyIRI: String): Long = {
    val entityRedisKey = String.format("%s:count:%s:outgoing:%s", parametersHash, propertyIRI, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val count = cachedDBpediaDF.filter($"sub" === resourceIRI && $"prop" === propertyIRI).count()
      RedisConnection.setValueInRedis(entityRedisKey, count)
      count
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns the number of RDF statements where the resource is the subject
   *
   * @param resourceIRI a resource IRI that will be used to get the content of the resource (RDF statements where the resource is the subject)
   * @return an integer representing
   */
  def getCountOfAllOutgoingLinksFromResource(resourceIRI: String): Long = {
    val entityRedisKey = String.format("%s:count:all:outgoing:%s", parametersHash, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val count = dbpediaDF.filter($"sub" === resourceIRI).count()
      RedisConnection.setValueInRedis(entityRedisKey, count)
      count
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns the total number of RDF statements in the knowledge base
   *
   * @return an integer representing the total number of RDF statements in the knowledge base
   */
  def getTotalNumberOfTriples: Long = triplesCount

  /**
   * Method that returns the total number of entities in the knowledge base
   *
   * @return an integer representing the total number of entities in the knowledge base
   */
  def getTotalNumberOfEntities: Long = entitiesCount

  /**
   * Method that returns the total number of RDF statements that involve the property
   *
   * @param propertyIRI a property IRI that will be used to filter statements
   * @return an integer representing the total number of RDF statements that involve the property
   */
  def getTotalNumberOfTriplesWithProperty(propertyIRI: String): Long = {
    val entityRedisKey = String.format("%s:count:triples:with:property:%s", parametersHash, propertyIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val count = dbpediaDF.filter($"prop" === propertyIRI).count()
      RedisConnection.setValueInRedis(entityRedisKey, count)
      count
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns the frequency of a property outgoing from an entity in the knowledge base
   *
   * @param resourceIRI a resource IRI that will be used to filter statements
   * @param propertyIRI a property IRI that will be used to filter statements
   * @return a double representing the frequency of a property outgoing from an entity in the knowledge base
   */
  def getFrequencyOfPropertyOutgoingFromResource(resourceIRI: String, propertyIRI: String): Double = {
    val entityRedisKey = String.format("%s:frequency:property:%s:outgoing:%s", parametersHash, propertyIRI, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val frequency = getCountOfPropertyOutgoingLinksFromResource(resourceIRI, propertyIRI).toDouble / getCountOfAllOutgoingLinksFromResource(resourceIRI)
      RedisConnection.setValueInRedis(entityRedisKey, frequency)
      frequency
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Double]
  }

  /**
   * Method that returns the frequency of a property incoming to an entity in the knowledge base
   *
   * @param resourceIRI a resource IRI that will be used to filter statements
   * @param propertyIRI a property IRI that will be used to filter statements
   * @return a double representing the frequency of a property incoming to an entity in the knowledge base
   */
  def getFrequencyOfPropertyIncomingToResource(resourceIRI: String, propertyIRI: String): Double = {
    val entityRedisKey = String.format("%s:frequency:property:%s:incoming:%s", parametersHash, propertyIRI, resourceIRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val frequency = getCountOfPropertyIncomingLinksToResource(resourceIRI, propertyIRI).toDouble / getCountOfAllIncomingLinksToResource(resourceIRI)
      RedisConnection.setValueInRedis(entityRedisKey, frequency)
      frequency
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Double]
  }

  /**
   * Method that returns the frequency of an entity in the knowledge base
   *
   * @param iri a resource IRI that will be used to filter statements
   * @return a double representing the frequency of an entity in the knowledge base
   */
  def getFrequencyOfEntity(iri: String): Double = {
    val entityRedisKey = String.format("%s:frequency:entity:%s", parametersHash, iri)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val frequency = dbpediaDF.filter($"sub" === iri || $"obj" === iri).count().toDouble / getTotalNumberOfTriples
      RedisConnection.setValueInRedis(entityRedisKey, frequency)
      frequency
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Double]
  }

  /**
   * Method that returns the frequency of a property in the knowledge base
   *
   * @param iri a property IRI that will be used to filter statements
   * @return a double representing the frequency of a property in the knowledge base
   */
  def getFrequencyOfProperty(iri: String): Double = {
    val entityRedisKey = String.format("%s:frequency:property:%s", parametersHash, iri)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val frequency = getTotalNumberOfTriplesWithProperty(iri).toDouble / getTotalNumberOfTriples
      RedisConnection.setValueInRedis(entityRedisKey, frequency)
      frequency
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Double]
  }

   /**
   * Method that returns the total number of co-occurrences of two properties
   *
   * @param property1IRI a property IRI that will be used to filter statements
   * @param property2IRI a property IRI that will be used to filter statements
   * @return an integer representing the total number of co-occurrences of two properties
   */
  def getCountOfPropertiesCoOccurrence(property1IRI: String, property2IRI: String): Long = {
    val entityRedisKey = String.format("%s:frequency:co-occurrence:%s:%s", parametersHash, property1IRI, property2IRI)
    if (RedisConnection.redisClient.get(entityRedisKey).isEmpty) {
      val pcoCount = cachedDBpediaDF.filter($"prop".isInCollection(Set(property1IRI, property2IRI)))
        .groupBy($"sub", $"obj")
        .agg(countDistinct($"prop").alias("count"))
        .filter($"count" > 1)
        .count()
      RedisConnection.setValueInRedis(entityRedisKey, pcoCount)
      pcoCount
    }
    else deserialize(RedisConnection.redisClient.get(entityRedisKey).get).asInstanceOf[Long]
  }

  /**
   * Method that returns True if a RDF statement exists in the knowledge base
   *
   * @param triple  a triple
   * @return a boolean representing if a RDF statement exists in the knowledge base
   */
  def statementExists(triple: Triple): Boolean = !cachedDBpediaDF.filter($"sub" === triple.sub && $"prop" === triple.prop && $"obj" === triple.obj).isEmpty

  /**
   * Method that checks for the appropriate direction of a triple and returns the triple in the appropriate direction
   *
   * @param triple  a triple
   * @return a triple in the appropriate direction
   */
  def getTripleCorrectDirection(triple: Triple): Triple = if (statementExists(triple)) triple else Triple(triple.obj, triple.prop, triple.sub)
}