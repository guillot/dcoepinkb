package parquetloader

import models.Triple
import org.apache.spark.sql.{DataFrame, SparkSession}

object ParquetLoader extends App {
  val spark: SparkSession = SparkSession
    .builder()
    .appName("Parquet Loader App")
    .master("local[*]")
    .getOrCreate()

  import spark.implicits._

  val mappingBasedObjectsDF: DataFrame = spark.sparkContext
    .textFile("src/main/resources/dbpedia/mappingbased-objects_lang=en.ttl")
    .map(_.split(" "))
    .map { case Array(sub, prop, obj, _) => Triple(sub.substring(1, sub.length - 1), prop.substring(1, prop.length - 1), obj.substring(1, obj.length - 1)) }
    .filter(_.obj.startsWith("http://"))
    .toDF("sub", "prop", "obj")

  val infoboxPropertiesDF: DataFrame = spark.sparkContext
    .textFile("src/main/resources/dbpedia/infobox-properties_lang=en.ttl")
    .map(_.split(" "))
    .filter(_.length == 4)
    .map { case Array(sub, prop, obj, _) => Triple(sub.substring(1, sub.length - 1), prop.substring(1, prop.length - 1), obj.substring(1, obj.length - 1)) }
    .filter(_.obj.startsWith("http://"))
    .toDF("sub", "prop", "obj")

  val dbpediaDF = mappingBasedObjectsDF.union(infoboxPropertiesDF)
    .dropDuplicates()

  dbpediaDF.write
    .option("compression", "snappy")
    .parquet("src/main/resources/dbpedia/dbpedia_45M.parquet")

  spark.stop()
}
