package utils

import com.redis.RedisClient
import main.MySparkSession.spark
import spark.implicits._
import org.apache.spark.sql.{DataFrame, Row}
import utils.Serialization.{deserialize, serialize}

object RedisConnection extends Serializable {
  private val REDIS_KEY_EXPIRATION_TIME = 604800 // 1 week

  lazy val redisClient: RedisClient = new RedisClient("localhost", 6379)

  def setValueInRedis(key: String, value: Any): Boolean = {
    RedisConnection.redisClient.set(key, serialize(value))
    RedisConnection.redisClient.expire(key, REDIS_KEY_EXPIRATION_TIME)
  }

  def getValueFromRedisAsDF(key: String, colNames: String*): DataFrame = {
    val rows = deserialize(RedisConnection.redisClient.get(key).get).asInstanceOf[Array[Row]]
    spark.sparkContext.parallelize(rows).map(row => (row.getString(0), row.getString(1))).toDF(colNames: _*)
  }
}
