package activationfunctions

import models.Similarity
import org.apache.commons.codec.digest.DigestUtils
import queryexecutor.SparkSQLQueryExecutor
import utils.RedisConnection
import utils.Serialization.deserialize

/**
 * The Jaccard index, also known as Intersection over Union and the Jaccard similarity coefficient,
 * measures similarity between finite sample sets, and is defined as
 * the size of the intersection divided by the size of the union of the sample sets.
 * Created by javier on 30/03/21.
 *
 * @author javier
 * @version 1.0 30/03/21
 * @see activationfunctions.EntitySimilarityMeasure
 */
class JaccardIndex(var sparkQueryExecutor: SparkSQLQueryExecutor, var activationFunctionArgs: List[String]) extends EntitySimilarityMeasure with Serializable {
  private val jaccardIndexMinValue: Double = if (activationFunctionArgs.nonEmpty && activationFunctionArgs.head.toDouble >= 0 && activationFunctionArgs.head.toDouble < 1) activationFunctionArgs.head.toDouble else 0.5
  private val depth: Int = if (activationFunctionArgs.size > 1 && activationFunctionArgs(1).toInt > 1) activationFunctionArgs(1).toInt else 1

  /**
   * Implementation of the method getSimilarity included in the activationfunctions.EntitySimilarityMeasure trait
   *
   * @param entity            an entity that will be compared with others to know their similarity
   * @param entitiesToCompare an array of strings that contains the IRIs of the neighbors of @param entity@param entitiesToCompare an array of strings that contains the IRIs of the neighbors of @param entity
   * @return an array containing the similarities between the @param entity and each entity in @param entitiesToCompare
   */
  def getSimilarity(entity: String, entitiesToCompare: Array[String]): Array[Similarity] = {
    val entityCharacteristicsSet = getCharacteristicsSetAtDepth(entity, depth, sparkQueryExecutor)
    val neighborsCharacteristicsSets = entitiesToCompare.map(neighbor => (neighbor, getCharacteristicsSetAtDepth(neighbor, depth, sparkQueryExecutor)))
    val similarities = neighborsCharacteristicsSets
      .map(r => Similarity(entity, r._1, getJaccardIndexSimilarity(entityCharacteristicsSet, r._2)))
      .filter(s => s.similarity >= jaccardIndexMinValue)
      .sortBy(_.similarity)(Ordering[Double].reverse)
    similarities
  }

  def getJaccardIndexSimilarity(as: Set[String], bs: Set[String]): Double = {
    val similarityHash: String = String.format("%s:Jaccard:%s", sparkQueryExecutor.parametersHash, DigestUtils.sha256Hex(String.format("%s:%s", as.toString, bs.toString)))
    if (RedisConnection.redisClient.get(similarityHash).isEmpty) {
      if (as.isEmpty || bs.isEmpty) { return 0.0 }
      val similarity = as.intersect(bs).size.toDouble / as.union(bs).size
      RedisConnection.setValueInRedis(similarityHash, similarity)
      similarity
    }
    else deserialize(RedisConnection.redisClient.get(similarityHash).get).asInstanceOf[Double]
  }
}