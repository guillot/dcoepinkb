package activationfunctions

import queryexecutor.SparkSQLQueryExecutor
import models.Similarity

/**
 * A trait with a method 'getSimilarity' to be implemented by those classes representing entity similarity measures
 * Created by javier on 30/03/21.
 *
 * @author javier
 * @version 1.0 30/03/21
 */
trait EntitySimilarityMeasure {
  /**
   * Method that returns an array of similarities between the @param entity and each entity in @param entitiesToCompare
   *
   * @param entity            an entity that will be compared with others to know their similarity
   * @param entitiesToCompare an array of strings that contains the IRIs of the neighbors of @param entity
   * @return an array containing the similarities between the @param entity and each entity in @param entitiesToCompare
   */
  def getSimilarity(entity: String, entitiesToCompare: Array[String]): Array[Similarity]

  def getCharacteristicsSetAtDepth(entity: String, depth: Int, sparkQueryExecutor: SparkSQLQueryExecutor): Set[String] = {
    sparkQueryExecutor.getReachableEntitiesFromResourceAtDepth(entity, depth)
  }
}
