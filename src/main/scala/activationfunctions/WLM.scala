package activationfunctions

import models.Similarity
import org.apache.commons.codec.digest.DigestUtils
import queryexecutor.SparkSQLQueryExecutor
import utils.RedisConnection
import utils.Serialization.deserialize

/**
 * The Wikipedia link-based measure (activationfunctions.WLM), was initially defined to compute the
 * similarity between articles in Wikipedia. However, this measure
 * can be adopted to measure the similarity between entities in any RDF graph.
 * Created by javier on 30/03/21.
 *
 * @author javier
 * @version 1.0 30/03/21
 * @see activationfunctions.EntitySimilarityMeasure
 */
class WLM(var sparkQueryExecutor: SparkSQLQueryExecutor, var activationFunctionArgs: List[String]) extends EntitySimilarityMeasure with Serializable {
  private val wlmMinValue: Double = if (activationFunctionArgs.nonEmpty && activationFunctionArgs.head.toDouble >= 0 && activationFunctionArgs.head.toDouble < 1) activationFunctionArgs.head.toDouble else 0.5
  private val depth: Int = if (activationFunctionArgs.size > 1 && activationFunctionArgs(1).toInt > 1) activationFunctionArgs(1).toInt else 1

  /**
   * Implementation of the method getSimilarity included in the activationfunctions.EntitySimilarityMeasure trait
   *
   * @param entity            an entity that will be compared with others to know their similarity
   * @param entitiesToCompare @param entitiesToCompare an array of strings that contains the IRIs of the neighbors of @param entity
   * @return an array containing the similarities between the @param entity and each entity in @param entitiesToCompare
   */
  def getSimilarity(entity: String, entitiesToCompare: Array[String]): Array[Similarity] = {
    val entityCharacteristicsSet = getCharacteristicsSetAtDepth(entity, depth, sparkQueryExecutor)
    val neighborsCharacteristicsSets = entitiesToCompare.map(neighbor => (neighbor, getCharacteristicsSetAtDepth(neighbor, depth, sparkQueryExecutor)))
    val similarities = neighborsCharacteristicsSets
      .map(r => Similarity(entity, r._1, getWLMSimilarity(entityCharacteristicsSet, r._2)))
      .filter(s => s.similarity >= wlmMinValue)
      .sortBy(_.similarity)(Ordering[Double].reverse)
    similarities
  }

  def getWLMSimilarity(as: Set[String], bs: Set[String]): Double = {
    val similarityHash: String = String.format("%s:WLM:%s", sparkQueryExecutor.parametersHash, DigestUtils.sha256Hex(String.format("%s:%s", as.toString, bs.toString)))
    if (RedisConnection.redisClient.get(similarityHash).isEmpty) {
      if (as.isEmpty || bs.isEmpty) { return 0.0 }
      val similarity = (Math.log(Math.max(as.size, bs.size)) - Math.log(as.intersect(bs).size)) / (Math.log(sparkQueryExecutor.getTotalNumberOfEntities) - Math.log(Math.min(as.size, bs.size)))
      RedisConnection.setValueInRedis(similarityHash, similarity)
      similarity
    }
    else deserialize(RedisConnection.redisClient.get(similarityHash).get).asInstanceOf[Double]
  }
}